// SPDX-FileCopyrightText: 2019 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

// base91 is a Go adaptation for the en-/decoding library to encoding binary data as ASCII.
// It is based on the C version from basE91 (http://base91.sourceforge.net/).
package base91 // import "catinello.eu/base91"

// Encode takes data and returns it in base91 encoded form.
// Please use the faster NewEncoder(dst io.Writer) io.WriteCloser implementation if possible.
func Encode(data []byte) []byte {
	var o []byte
	var b, n uint
	var v int

	for i := 0; i < len(data); i++ {
		b |= uint((data)[i]) << n
		n += 8

		if n > 13 {
			v = int(b) & 8191

			if v > 88 {
				b >>= 13
				n -= 13
			} else {
				v = int(b) & 16383
				b >>= 14
				n -= 14
			}

			o = append(o, enctab[v%91], enctab[v/91])
		}
	}

	if n > 0 {
		o = append(o, enctab[b%91])

		if n > 7 || b > 90 {
			o = append(o, enctab[b/91])
		}
	}

	return o
}

// Decode takes base91 data and returns it in decoded form.
// Please use the faster NewDecoder(src io.Reader) io.Reader implementation if possible.
func Decode(data []byte) []byte {
	if len(data) == 0 {
		return []byte{}
	}

	var o []byte
	var b, n uint
	var v int = -1

	for i := 0; i < len(data); i++ {
		c := dectab[(data)[i]]

		if c > 90 {
			continue
		}

		if v < 0 {
			v = int(c)
			continue
		}

		v += int(c) * 91
		b |= uint(v) << n

		if v&8191 > 88 {
			n += 13
		} else {
			n += 14
		}

		for {
			o = append(o, byte(b&255))
			b >>= 8
			n -= 8

			if n <= 7 {
				break
			}
		}

		v = -1
	}

	if v > -1 {
		o = append(o, byte((b|uint(v)<<n)&255))
	}

	return o
}
