// SPDX-FileCopyrightText: 2021 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package base91

import (
	"io"
)

type decoder struct {
	src io.Reader

	n uint
	b uint
	v int

	in  int
	out int
}

// NewDecoder takes an io.Reader as source to read encoded data from.
func NewDecoder(src io.Reader) io.Reader {
	return &decoder{src: src, v: -1}
}

// Read decodes data from decoder's src and reads it into p.
func (d *decoder) Read(p []byte) (int, error) {
	var buf []byte

	n, err := d.src.Read(p)
	if err != io.EOF && err != nil {
		return n, err
	} else if err == io.EOF {
		buf = d.end()
	} else {
		var start, stop, rest, njobs int
		var bsize int = ((32 * 1024) - 1) << 3
		bsize /= 15

		if bsize >= n {
			bsize = n
			njobs = 1
		} else {
			njobs += n / bsize
			rest = n % bsize
			if rest > 0 {
				njobs++
			}
		}

		if njobs == 1 {
			buf = append(buf, d.decode(p[:n])...)
		} else {
			for i := 0; i < njobs; i++ {
				if i != 0 {
					start += bsize
				}

				if i < njobs-1 {
					stop += bsize
				} else {
					stop += rest
				}

				buf = append(buf, d.decode(p[start:stop])...)
			}
		}
	}

	w := copy(p, buf)

	return w, err

}

func (d *decoder) decode(b []byte) []byte {
	var o []byte

	for i := 0; i < len(b); i++ {
		d.in++
		c := dectab[b[i]]

		if c > 90 {
			continue
		}

		if d.v < 0 {
			d.v = int(c)
			continue
		}

		d.v += int(c) * 91
		d.b |= uint(d.v) << d.n

		if d.v&8191 > 88 {
			d.n += 13
		} else {
			d.n += 14
		}

		for {
			o = append(o, byte(d.b&255))
			d.b >>= 8
			d.n -= 8

			if d.n <= 7 {
				break
			}
		}

		d.v = -1
	}

	d.out += len(o)
	return o
}

func (d *decoder) end() []byte {
	var o []byte

	if d.v > -1 {
		o = append(o, byte((d.b|uint(d.v)<<d.n)&255))
	}

	d.out += len(o)

	return o
}
