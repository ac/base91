module catinello.eu/base91/cmd/base91

go 1.23.1

require (
	catinello.eu/base91 v0.0.0-20241213181057-6966898c003d
	catinello.eu/capabilities v0.0.0-20241111172555-e4ed37e59a5a
	catinello.eu/cli v0.0.0-20241111163805-1af155a17445
	catinello.eu/com v0.0.0-20241117145441-ff2de89e869d
	catinello.eu/restrict v0.0.0-20241128173941-51521c72394c
)

require (
	catinello.eu/inet v0.0.0-20241127115856-0e99a8ad6f71 // indirect
	catinello.eu/store v0.0.0-20241124103751-7f3958b0404f // indirect
	github.com/seccomp/libseccomp-golang v0.10.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	kernel.org/pub/linux/libs/security/libcap/cap v1.2.73 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.73 // indirect
)
