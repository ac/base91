// SPDX-FileCopyrightText: 2019 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"os"

	"catinello.eu/cli"
	"catinello.eu/com"
)

func direct(x cli.Data) error {
	c := x.Pass.(*com.Config)

	switch x.Name() {
	case "help":
		help(c)
		os.Exit(0)
	case "license":
		c.Print(license)
		os.Exit(0)
	case "version":
		c.Println(version)
		os.Exit(0)
	}

	return nil
}

func help(c *com.Config) {
	c.Println("base91 - En-/decoding from data to/from baseE91 text.")
	c.Println()
	c.Println("Usage:")
	c.Println("  base91 [OPTIONS] <FILE>")
	c.Println()
	c.Println("Options:")
	c.Println("  -d | --decode <FILE> / -     | Decode from file or from stdin.")
	c.Println("  -w | --wrap n                | Wrap line after n.")
	c.Println()
	c.Println("  --help                       | Show this help.")
	c.Println("  --license                    | Print license.")
	c.Println("  --version                    | Print version.")
	c.Println()
	c.Println("Examples:")
	c.Println("  echo -n \"bla\"  | base91      | Encode")
	c.Println("  echo -n \"<izI\" | base91 -d - | Decode")
	c.Println()
	c.Println("Website:")
	c.Println("  https://catinello.eu/base91")
	c.Println()
	c.Println("License:")
	c.Println("  BSD-3-Clause License © 2019 Antonino Catinello")
	c.Println()
	c.Println("Version:")
	c.Println("  " + version)
}
