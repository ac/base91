// SPDX-FileCopyrightText: 2019 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

// Command line tool to de-/encode base91.
package main

import (
	_ "embed"
	"io"
	"os"

	"catinello.eu/base91"
	"catinello.eu/capabilities"
	"catinello.eu/cli"
	"catinello.eu/com"
	"catinello.eu/restrict"
)

//go:embed LICENSE
var license string

var version string

var wrap uint = 76

func main() {
	c := com.New(com.Common)
	o := cli.New()

	if err := capabilities.Set(""); err != nil {
		c.E().Println(err)
		os.Exit(254)
	}

	if err := restrict.Syscalls("stdio rpath unveil"); err != nil {
		c.E().Println(err)
		os.Exit(255)
	}

	o.Add(cli.Bool, 0, "version").Pass(cli.Data{Pass: c}).Direct(direct)
	o.Add(cli.Bool, 0, "license").Pass(cli.Data{Pass: c}).Direct(direct)
	o.Add(cli.Bool, 0, "help").Pass(cli.Data{Pass: c}).Direct(direct)
	o.Add(cli.Int, 'w', "wrap")
	o.Add(cli.String, 'd', "decode")

	if err := o.Args(); err != nil {
		c.E().Println(err)
		os.Exit(1)
	}

	if o.HasValue("wrap") {
		w := o.Int("wrap")
		if w > 0 {
			wrap = uint(w)
		}
	}

	base91.LineBreakAfter = wrap

	if o.HasValue("decode") {
		d := o.String("decode")
		if d != "-" {
			if err := restrict.Access(d, "r"); err != nil {
				c.E().Println(err)
				os.Exit(255)
			}

			if err := restrict.AccessLock(); err != nil {
				c.E().Println(err)
				os.Exit(255)
			}

			content, err := os.Open(d)
			if err != nil {
				c.E().Println(err)
				os.Exit(2)
			}
			defer content.Close()

			d := base91.NewDecoder(content)
			_, err = io.Copy(os.Stdout, d)
			if err != nil {
				c.E().Println(err)
				os.Exit(1)
			}
		} else {
			if err := restrict.Syscalls("stdio"); err != nil {
				c.E().Println(err)
				os.Exit(255)
			}

			d := base91.NewDecoder(os.Stdin)
			if _, err := io.Copy(os.Stdout, d); err != nil {
				c.E().Println(err)
				os.Exit(1)
			}

		}

		os.Exit(0)
	}

	if len(o.Ignored()) > 1 {
		help(c)
		os.Exit(1)
	}

	if len(o.Ignored()) == 1 {
		f := o.Ignored()[0]
		if f != "-" {
			if err := restrict.Access(f, "r"); err != nil {
				c.E().Println(err)
				os.Exit(255)
			}

			if err := restrict.AccessLock(); err != nil {
				c.E().Println(err)
				os.Exit(255)
			}

			content, err := os.Open(f)
			if err != nil {
				c.E().Println(err)
				os.Exit(1)
			}
			defer content.Close()

			e := base91.NewEncoder(os.Stdout)

			if _, err := io.Copy(e, content); err != nil {
				c.E().Println(err)
				os.Exit(1)
			}

			e.Close()
			c.Println()
			os.Exit(0)
		}
	}

	if err := restrict.Syscalls("stdio"); err != nil {
		c.E().Println(err)
		os.Exit(255)
	}

	if isPipe() {
		e := base91.NewEncoder(os.Stdout)
		if _, err := io.Copy(e, os.Stdin); err != nil {
			c.E().Println(err)
			os.Exit(1)
		}

		e.Close()
		c.Println()
	} else {
		help(c)
	}
}

func isPipe() bool {
	fi, _ := os.Stdin.Stat()
	if (fi.Mode() & os.ModeCharDevice) == 0 {
		return true
	} else {
		return false
	}
}
