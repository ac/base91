// SPDX-FileCopyrightText: 2024 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package base91_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"

	"catinello.eu/base91"
)

func ExampleEncode() {
	in := []byte("ABC_def→1234567890")
	out := base91.Encode(in)

	fmt.Println(string(out))
	// Output: fGn/1+~j/nFa!YAS77Z.wnXBD
}

func ExampleDecode() {
	in := []byte("fGn/1+~j/nFa!YAS77Z.wnXBD")
	out := base91.Decode(in)

	fmt.Println(string(out))
	// Output: ABC_def→1234567890
}

func ExampleNewEncoder() {
	var sample []byte = []byte("ABC_def→1234567890")
	w := &bytes.Buffer{}
	e := base91.NewEncoder(w)
	_, err := e.Write(sample)
	if err != nil {
		log.Fatalf("Error encoding `%s`", sample)
	}

	err = e.Close()
	if err != nil {
		log.Fatalf("Error encoding `%s`", sample)
	}

	fmt.Println(w.String())
	// Output: fGn/1+~j/nFa!YAS77Z.wnXBD
}

func ExampleNewDecoder() {
	sample := []byte("fGn/1+~j/nFa!YAS77Z.wnXBD")
	r := bytes.NewReader(sample)

	d := base91.NewDecoder(r)

	var w []byte

	_, err := d.Read(w)
	if err != nil {
		log.Fatalf("Error decoding `%s`", sample)
	}

	buf, err := ioutil.ReadAll(d)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(buf))
	// Output: ABC_def→1234567890
}
