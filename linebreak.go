// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package base91 // import "catinello.eu/base91"

func (e *encoder) newline(o *[]byte) {
	if uint(e.c) == LineBreakAfter {
		*o = append(*o, []byte("\n")...)
		e.c = 0
	}
}
