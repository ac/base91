// SPDX-FileCopyrightText: 2021 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package base91 // import "catinello.eu/base91"

import (
	"bytes"
	"io"
	"strings"
	"testing"
)

var samples = map[string]string{
	"1":                           "xA",
	"1234567890":                  "QztEml0o[2;(A",
	"abcdefghijklmnopqurstuvwxyz": "#G(Ic,5ph#77&xrmlrjg2]jTs%2<WF%qfB",
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,./:;<=>?@[]^_`{|}~\"": "fG^F%w_o%5qOdwQbFrzd[5eYAP;gMP+f#G(Ic,5ph#77&xrmlrjgs@DZ7UB>xQGrgw_,$k_i$Js@Tj$MaRDa7dq)L1<[3vwV[|O/7%q{{9G`C/LM",
	"new line\ntest\n": "ZPtKE<H[;RS/Tm{oKA",
	"←↓→":              "hT%5<EtO%X/",
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz": "fG^F%w_o%5qOdwQbFrzd[5eYAP;gMP+f#G(Ic,5ph#77&xrmlrjgs@DZ7UB>xQGrfG^F%w_o%5qOdwQbFrzd[5eYAP;gMP+f#G(Ic,5ph#77&xrmlrjgs@DZ7UB>xQGr",
	"ABC_def→1234567890": "fGn/1+~j/nFa!YAS77Z.wnXBD",
}

func TestEncode(t *testing.T) {
	for k, v := range samples {
		LineBreakAfter = 0
		e := Encode([]byte(k))

		if strings.Compare(string(e), v) != 0 {
			t.Errorf("Incorrect encoding of `%s`\n got `%s`\n expected `%s`", k, e, v)
		}
	}
}

func TestDecode(t *testing.T) {
	for k, v := range samples {
		d := Decode([]byte(v))

		if strings.Compare(string(d), k) != 0 {
			t.Errorf("Incorrect decoding of `%s`\n got `%s`\n expected `%s`", v, d, k)
		}
	}
}

func TestNewEncoder(t *testing.T) {
	// testing newlines
	//	samples["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,./:;<=>?@[]^_`{|}~\""] = "fG^F%w_o%5qOdwQbFrzd[5eYAP;gMP+f#G(Ic,5ph#77&xrmlrjgs@DZ7UB>xQGrgw_,$k_i$Js@\nTj$MaRDa7dq)L1<[3vwV[|O/7%q{{9G`C/LM"
	//	samples["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"] = "fG^F%w_o%5qOdwQbFrzd[5eYAP;gMP+f#G(Ic,5ph#77&xrmlrjgs@DZ7UB>xQGrfG^F%w_o%5qO\ndwQbFrzd[5eYAP;gMP+f#G(Ic,5ph#77&xrmlrjgs@DZ7UB>xQGr"

	for k, v := range samples {
		w := &bytes.Buffer{}
		e := NewEncoder(w)
		_, err := e.Write([]byte(k))
		if err != nil {
			t.Errorf("Error encoding `%s`", k)
		}

		err = e.Close()
		if err != nil {
			t.Errorf("Error encoding `%s`", k)
		}

		if len(w.Bytes()) != len(v) {
			t.Errorf("Incorrect output length of `%s` got %v expected %v", k, len(w.Bytes()), len(v))
		}

		if w.String() != v {
			t.Errorf("Incorrect encoding of `%s`\n got `%s`\n expected `%s`", k, w.String(), v)
		}
	}
}

func TestNewDecoder(t *testing.T) {
	/*	LineBreakAfter = 4

		var samples = map[string]string{
			"1234567890": "QztE\nml0o\n[2;(A",
		}
	*/
	for k, v := range samples {
		r := strings.NewReader(v)
		w := &bytes.Buffer{}
		d := NewDecoder(r)
		n, err := io.Copy(w, d)
		if err != nil {
			t.Errorf("Error decoding `%s`", v)
		}

		if int(n) != len(w.Bytes()) {
			t.Errorf("Incorrect output length of `%s` got %v expected %v", v, len(w.Bytes()), n)
		}

		if w.String() != k {
			t.Errorf("Incorrect decoding of `%s`\n got `%s`\n expected `%s`", v, w.String(), k)
		}
	}
}
