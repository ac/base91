// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package base91

import (
	"io"
)

type encoder struct {
	dst io.Writer

	n uint
	b uint
	v int

	wrap bool
	c    int

	in  int
	out int

	bufsize int
}

var BufferSize int = (((32 * 1024) - 2) << 4) / 29

// NewEncoder takes an io.Writer as destination to write encoded data to.
func NewEncoder(dst io.Writer) io.WriteCloser {
	if LineBreakAfter > 0 {
		if LineBreakAfter%2 != 0 {
			// fallback
			LineBreakAfter -= LineBreakAfter % 2
		}

		return &encoder{dst: dst, wrap: true, bufsize: BufferSize}
	} else {
		return &encoder{dst: dst, wrap: false, bufsize: BufferSize}
	}
}

// Write takes data p and encodes it to encoder's dst.
func (e *encoder) Write(p []byte) (int, error) {
	var buf []byte
	var start, stop, rest, njobs int
	var bsize int = e.bufsize

	n := len(p)

	if bsize > n {
		bsize = n
		njobs = 1
	} else {
		njobs += n / bsize
		rest = n % bsize
		if rest > 0 {
			njobs++
		}
	}

	if njobs == 1 {
		buf = append(buf, e.encode(p[:n])...)
	} else {
		for i := 0; i < njobs; i++ {
			if i != 0 {
				start += bsize
			}

			if i < njobs-1 {
				stop += bsize
			} else {
				stop += rest
			}

			buf = append(buf, e.encode(p[start:stop])...)
		}
	}

	x, err := e.dst.Write(buf)
	e.out += x

	return len(p), err
}

func (e *encoder) encode(p []byte) []byte {
	var o []byte

	for i := 0; i < len(p); i++ {
		e.in++
		e.b |= uint(p[i]) << e.n
		e.n += 8

		if e.n > 13 {
			e.v = int(e.b) & 8191

			if e.v > 88 {
				e.b >>= 13
				e.n -= 13
			} else {
				e.v = int(e.b) & 16383
				e.b >>= 14
				e.n -= 14
			}

			o = append(o, enctab[e.v%91], enctab[e.v/91])

			if e.wrap {
				e.c += 2
				e.newline(&o)
			}

		}
	}

	return o
}

// Close ends the encoders input.
func (e *encoder) Close() error {
	var o []byte

	if e.n > 0 {
		o = append(o, enctab[e.b%91])

		if e.wrap {
			e.c++
			e.newline(&o)
		}

		if e.n > 7 || e.b > 90 {
			o = append(o, enctab[e.b/91])

			if e.wrap {
				e.c++
				e.newline(&o)
			}
		}
	}

	if e.wrap && e.c > 0 {
		e.newline(&o)
	}

	x, err := e.dst.Write(o)
	e.out += x

	return err
}
