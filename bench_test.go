// SPDX-FileCopyrightText: 2021 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package base91 // import "catinello.eu/base91"

import (
	"bytes"
	"crypto/rand"
	"encoding/ascii85"
	"encoding/base64"
	"io"
	"testing"
)

func BenchmarkEncoderBase91(b *testing.B) {
	s := make([]byte, 1024*1024)
	if _, err := rand.Read(s); err != nil {
		b.Fatal(err)
	}

	r := bytes.NewReader(s)

	buf := &bytes.Buffer{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		e := NewEncoder(buf)

		_, err := io.Copy(e, r)
		if err != nil {
			b.Fatal(err)
		}

		err = e.Close()
		if err != nil {
			b.Fatal(err)
		}
	}

	_ = buf
}

func BenchmarkDecoderBase91(b *testing.B) {
	s := make([]byte, 1024*1024)
	if _, err := rand.Read(s); err != nil {
		b.Fatal(err)
	}

	r := &bytes.Buffer{}
	e := NewEncoder(r)
	e.Write(s)
	e.Close()

	buf := &bytes.Buffer{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d := NewDecoder(r)
		_, err := io.Copy(buf, d)
		if err != nil {
			b.Fatal(err)
		}
	}

	_ = buf
}

func BenchmarkEncoderBase64(b *testing.B) {
	s := make([]byte, 1024*1024)
	if _, err := rand.Read(s); err != nil {
		b.Fatal(err)
	}

	r := bytes.NewReader(s)

	buf := &bytes.Buffer{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		e := base64.NewEncoder(base64.StdEncoding, buf)

		_, err := io.Copy(e, r)
		if err != nil {
			b.Fatal(err)
		}

		err = e.Close()
		if err != nil {
			b.Fatal(err)
		}
	}

	_ = buf
}

func BenchmarkDecoderBase64(b *testing.B) {
	s := make([]byte, 1024*1024)
	if _, err := rand.Read(s); err != nil {
		b.Fatal(err)
	}

	r := &bytes.Buffer{}
	e := base64.NewEncoder(base64.StdEncoding, r)
	e.Write(s)
	e.Close()

	buf := &bytes.Buffer{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d := base64.NewDecoder(base64.StdEncoding, r)

		_, err := io.Copy(buf, d)
		if err != nil {
			b.Fatal(err)
		}
	}

	_ = buf
}

func BenchmarkEncoderAscii85(b *testing.B) {
	s := make([]byte, 1024*1024)
	if _, err := rand.Read(s); err != nil {
		b.Fatal(err)
	}

	r := bytes.NewReader(s)

	buf := &bytes.Buffer{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		e := ascii85.NewEncoder(buf)

		_, err := io.Copy(e, r)
		if err != nil {
			b.Fatal(err)
		}

		err = e.Close()
		if err != nil {
			b.Fatal(err)
		}
	}

	_ = buf
}

func BenchmarkDecoderAscii85(b *testing.B) {
	s := make([]byte, 1024*1024)
	if _, err := rand.Read(s); err != nil {
		b.Fatal(err)
	}

	r := &bytes.Buffer{}
	e := ascii85.NewEncoder(r)
	e.Write(s)
	e.Close()

	buf := &bytes.Buffer{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d := ascii85.NewDecoder(r)

		_, err := io.Copy(buf, d)
		if err != nil {
			b.Fatal(err)
		}
	}

	_ = buf
}
